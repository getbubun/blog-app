import { Component, OnInit, OnDestroy } from '@angular/core';
//importing route related code
import { ActivatedRoute, Router } from '@angular/router';
import { BlogService } from '../blog.service';
import { BlogHttpService } from '../blog-http.service';
import { Location } from '@angular/common'


@Component({
  selector: 'app-blog-view', 
  templateUrl: './blog-view.component.html',
  styleUrls: ['./blog-view.component.css'],
  providers:[Location]
  
})
export class BlogViewComponent implements OnInit, OnDestroy {

  
  public currentBlog;


  constructor(private _route: ActivatedRoute, private router:Router,public blogService:BlogService, public blogHttpService:BlogHttpService, private location: Location) {

    console.log("Blog-view Constructor is called");
   }

  ngOnInit() {

  console.log("Blog-view OnInitCalled");
  let myBlogId =  this._route.snapshot.paramMap.get('blogId');
  console.log(myBlogId);

  
  this.blogHttpService.getSingleBlogInformation(myBlogId).subscribe(
    data =>{
      console.log(data);
      this.currentBlog = data["data"];
    },
    error =>{
      console.log("some error occurrd")
      console.log(error.errorMessage)
    }
  )
} 

public deleteThisBlog():any{
  this.blogHttpService.deleteBlog(this.currentBlog.blogId).subscribe(

    data =>{
      console.log(data);
      console.log('Blog deleted Successfully');
      setTimeout(() => {
        this.router.navigate(['/home']);
      },1000)
    },
    error =>{
      console.log("Some error occured");
      console.log(error.errorMessage);
      alert("Some error occured");
    }
  )
}

public gobackToPreviousPage(): any{
  this.location.back();
}

  ngOnDestroy(){
    console.log(" blog-view OnDestroyCalled");
  
  }

 

 

}
