import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  
   
  public allBlogs = []

  public currentBlog;
  
  constructor() {

    console.log("Service constructor is called !") 
   }


   // method to return all blog
  public getAllBlogs() : any {

    return this.allBlogs;

  }

   // method to get a particular blog

  public getSingleBlogInformation(currentBlogId): any {

    for(let blog of this.allBlogs){
      if(blog.blogId == currentBlogId){
        this.currentBlog = blog;
      }
    }

    console.log(this.currentBlog);
    return this.currentBlog;
  }

 
}
