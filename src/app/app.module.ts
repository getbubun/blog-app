import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';



import { AppComponent } from './app.component';
import { HomeComponent} from './home/home.component';
import { AboutComponent } from './about/about.component';
import { BlogCreateComponent } from './blog-create/blog-create.component';
import { BlogViewComponent } from './blog-view/blog-view.component';
import { BlogEditComponent } from './blog-edit/blog-edit.component';
import { NotFoundComponent } from './not-found/not-found.component'
//import statement for blog service
import { BlogService } from './blog.service';
import { BlogHttpService } from './blog-http.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    BlogCreateComponent,
    BlogViewComponent,
    BlogEditComponent,
    NotFoundComponent
  ],

  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
   
    RouterModule.forRoot([
      {path:'home',component:HomeComponent},
      {path:'', redirectTo :'home',pathMatch:'full'},
      {path:'about',component:AboutComponent},
      {path:'blog/:blogId',component:BlogViewComponent},
      {path:'create',component:BlogCreateComponent},
      {path:'edit/:blogId',component:BlogEditComponent},
      {path:'**',component:NotFoundComponent}


    
    ])

  ],
  providers: [BlogService, BlogHttpService,HttpClientModule],

  bootstrap: [AppComponent]
})
export class AppModule { }
