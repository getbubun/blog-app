import { Injectable } from '@angular/core';
//importing http client
import {HttpClient, HttpErrorResponse} from '@angular/common/http';

import { Observable } from 'rxjs' ;
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class BlogHttpService {
  
  public allBlogs;
  public currentBlog;
  public baseUrl ='https://blogapp.edwisor.com/api/v1/blogs';
  public authToken = 'YzhiMmYzMDdkY2Q4YmM1ZmZlMzA3ZTFmNGQ3ZTdhMTcyZTVjMjI4MGViMGI5ZGZlYTJkYmYwY2UwOTg1MzQ2M2U3YjFkMzhlYmE5ODk1ZDc4YmJiOWFlODFjZDdmYTIzMTk1YmQ0Yjg3ZGRkZmQ2YzZmZGU0NDdmYWVjOTgwNzRkOQ=='

  constructor(private  _http:HttpClient) {
    console.log("Blog Http service was called");

   }

   //exception handler
   private handleError(err:HttpErrorResponse){
     console.log('Handle error Http call');
     console.log(err.message);
     return Observable.throw(err.message)
   }

    // method to return all blog
  public getAllBlogs() : any {
    let myResponse = this._http.get(this.baseUrl + '/all?authToken=' + this.authToken);
    console.log(myResponse);
    return myResponse;
    

  }

   // method to get a particular blog 

  public getSingleBlogInformation(currentBlogId): any {
    let myResponse = this._http.get(this.baseUrl + '/view' + '/' + currentBlogId + '?authToken=' + this.authToken);
    return myResponse;
    
  }

  //method to create a blog
  public createBlog(blogData) : any{
    let myResponse = this._http.post(this.baseUrl + '/create' + '?authToken=' + this.authToken, blogData);
    return myResponse;
  }

  //method to delete a blog 
  public deleteBlog(blogId) : any{
    let data ={}
    let myResponse = this._http.post(this.baseUrl + '/' + blogId + '/delete' + '?authToken=' + this.authToken, data);
    return myResponse;
  }

  //method to edit a blog 
  public editBlog(blogId, blogData) :any{
    let myResponse = this._http.put(this.baseUrl + '/' + blogId + '/edit' + '?authToken=' + this.authToken, blogData );
    return myResponse;
  }
}
